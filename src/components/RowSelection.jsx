import { useMemo } from "react"
import { COLUMNS } from "./columns"
import MOCK_DATA from './MOCK_DATA.json'
import { useTable, useRowSelect } from 'react-table'
import './table.css'
import { MyCheckbox } from "./MyCheckbox"

export const RowSelection = () => {
    const columns = useMemo(() => COLUMNS, [])
    const data = useMemo(() => MOCK_DATA, [])

    const pushToVisibleColumns = (hooks) => {
        hooks.visibleColumns.push(columns => {
            return [
                {
                    id: 'selection',
                    Header: ({ getToggleAllRowsSelectedProps }) => (
                        <MyCheckbox {...getToggleAllRowsSelectedProps()} />
                    ),
                    Cell: ({ row }) => (
                        <MyCheckbox {...row.getToggleRowSelectedProps()} />
                    )
                },
                ...columns
            ]
        })
    }

    const tableInstance = useTable(
        { columns, data },
        useRowSelect,
        pushToVisibleColumns
    )

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        selectedFlatRows
    } = tableInstance

    const firstPageRows = rows.slice(0, 10)

    return (
        <>
            <table {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {firstPageRows.map(row => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell =>
                                    <td {...cell.getCellProps()}>
                                        {cell.render('Cell')}
                                    </td>
                                )}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <pre>
                <code>
                    {JSON.stringify(
                        {
                            selectedFlatRows: selectedFlatRows.map(row => row.original)
                        },
                        null,
                        2
                    )}
                </code>
            </pre>
        </>
    )
}
