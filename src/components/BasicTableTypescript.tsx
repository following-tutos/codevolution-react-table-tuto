import { useMemo } from "react"
import MOCK_DATA from './MOCK_DATA.json'
import { Column, TableOptions, useTable } from 'react-table'
import './table.css'

// Initially using .tsx but moved to .jsx due to a type error:
/*
Type '{ Header: string; accessor: string; }[]' is not assignable to type 'readonly Column<{ id: number; first_name: string; last_name: string; email: string; date_of_birth: strin>[]'.
  Type '{ Header: string; accessor: string; }' is not assignable to type 'Column<{ id: number; first_name: string; last_name: string; email: string; date_of_birth: string; age: n
    Type '{ Header: string; accessor: string; }' is not assignable to type 'ColumnInterface<{ id: number; first_name: string; last_name: string; email: string; date_of_birth: str }> & { accessor: "age"; } & ColumnInterfaceBasedOnValue<...>'.
      Type '{ Header: string; accessor: string; }' is not assignable to type '{ accessor: "age"; }'.
        Types of property 'accessor' are incompatible.
          Type 'string' is not assignable to type '"age"'.  TS2322

        |    const tableInstance = useTable({
        |         columns: columns,
        |         ^
        |         data: data
        |     })
*/
// So, to solve that, we need to use types :)

interface IData {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    date_of_birth: string;
    age: number;
    country: string;
    phone: string;
}

const TYPED_COLUMNS: Column<IData>[] = [
    {
        Header: 'Id',
        accessor: 'id'
    },
    {
        Header: 'First Name',
        accessor: 'first_name'
    }
]

export const BasicTableTypescript = () => {
    const memoColumns = useMemo<Column<IData>[]>(
        () => TYPED_COLUMNS,
        []
    )
    const memoData = useMemo<IData[]>(
        () => MOCK_DATA,
        []
    )

    const tableOptions: TableOptions<IData> = {
        columns: memoColumns,
        data: memoData
    }

    const tableInstance = useTable(tableOptions)

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = tableInstance

    return (
        <table {...getTableProps()}>
            <thead>
                {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map(column => (
                            <th {...column.getHeaderProps()}>
                                {column.render('Header')}
                            </th>
                        ))}
                    </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {rows.map(row => {
                    prepareRow(row)
                    return (
                        <tr {...row.getRowProps()}>
                            {row.cells.map(cell =>
                                <td {...cell.getCellProps()}>
                                    {cell.render('Cell')}
                                </td>
                            )}
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}
