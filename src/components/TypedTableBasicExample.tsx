// Basic example of the workaround to use .tsx
// Extracted from https://github.com/TanStack/react-table/issues/2912#issuecomment-747398841

import { useMemo } from "react"
import { Column, useTable } from 'react-table'


interface IData {
    col1: string;
    col2: number;
}

export function TypedTableBasicExample() {
    const memoData = useMemo<IData[]>(
        () => [
            {
                col1: "Hello",
                col2: 1
            },
            {
                col1: "react-table",
                col2: 2
            },
            {
                col1: "whatever",
                col2: 3
            }
        ],
        []
    )

    const memoColumns = useMemo<Column<IData>[]>(
        () => [
            {
                Header: "Column 1",
                accessor: "col1"
            },
            {
                Header: "Column 1",
                accessor: "col2"
            }
        ],
        []
    )

    const tableInstance = useTable({
        columns: memoColumns,
        data: memoData
    })

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = tableInstance

    return (
        <table {...getTableProps()}>
            <thead>
                {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map(column => (
                            <th {...column.getHeaderProps()}>
                                {column.render('Header')}
                            </th>
                        ))}
                    </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {rows.map(row => {
                    prepareRow(row)
                    return (
                        <tr {...row.getRowProps()}>
                            {row.cells.map(cell =>
                                <td {...cell.getCellProps()}>
                                    {cell.render('Cell')}
                                </td>
                            )}
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}